﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace LibVirtADL.Native
{
    public abstract unsafe class LibVirtCommon
    {
        public delegate void virFreeCallback(IntPtr opaque);

        public enum virConnectCloseReason {
            /// <summary>
            /// Misc I/O error
            /// </summary>
            VIR_CONNECT_CLOSE_REASON_ERROR     = 0,
            /// <summary>
            /// End-of-file from server
            /// </summary>
            VIR_CONNECT_CLOSE_REASON_EOF       = 1,
            /// <summary>
            /// Keepalive timer triggered
            /// </summary>
            VIR_CONNECT_CLOSE_REASON_KEEPALIVE = 2,
            /// <summary>
            /// Client requested it
            /// </summary>
            VIR_CONNECT_CLOSE_REASON_CLIENT    = 3,
        }
        
        /// <summary>
        /// Express the type of a virTypedParameter
        /// </summary>
        public enum virTypedParameterType {
            /// <summary>
            /// integer case
            /// </summary>
            VIR_TYPED_PARAM_INT     = 1,
            /// <summary>
            /// unsigned integer case
            /// </summary>
            VIR_TYPED_PARAM_UINT    = 2,
            /// <summary>
            /// long long case
            /// </summary>
            VIR_TYPED_PARAM_LLONG   = 3,
            /// <summary>
            /// unsigned long long case
            /// </summary>
            VIR_TYPED_PARAM_ULLONG  = 4,
            /// <summary>
            /// double case
            /// </summary>
            VIR_TYPED_PARAM_DOUBLE  = 5,
            /// <summary>
            /// boolean(character) case
            /// </summary>
            VIR_TYPED_PARAM_BOOLEAN = 6,
            /// <summary>
            /// string case
            /// </summary>
            VIR_TYPED_PARAM_STRING  = 7,
        }

        /// <summary>
        /// Flags related to libvirt APIs that use virTypedParameter.
        /// </summary>
        public enum virTypedParameterFlags{
            VIR_TYPED_PARAM_STRING_OKAY = 1 << 2,
        }

        /// <summary>
        /// The field length constant of virTypedParameter name
        /// </summary>
        public const int VIR_TYPED_PARAM_FIELD_LENGTH = 80;


        /// <summary>
        /// A named parameter, including a type and value.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct _virTypedParameter {
            /// <summary>
            /// parameter name
            /// </summary>
            public fixed byte field[VIR_TYPED_PARAM_FIELD_LENGTH];
            /// <summary>
            /// parameter type, virTypedParameterType
            /// </summary>
            public virTypedParameterType type;
            public struct _virTypedParameterValueUnion {
                public int i;
                public uint ui;
                public long l;
                public ulong ul;
                public double d;
                public char b;
                public char *s;
            }
            /// <summary>
            /// Parameter value
            /// </summary>
            public _virTypedParameterValueUnion value;
        }
        [return: MarshalAs(UnmanagedType.LPStruct)]
        public abstract _virTypedParameter virTypedparametersGet(_virTypedParameter* parameters, int nparameters, StringBuilder name);
        public abstract int virTypedparametersGetInt(_virTypedParameter* parameters, int nparameters, StringBuilder name, ref int value);
        public abstract int virTypedparametersGetUInt(_virTypedParameter* parameters, int nparameters, StringBuilder name, ref uint value);
        public abstract int virTypedparametersGetLLong(_virTypedParameter* parameters, int nparameters, StringBuilder name, ref long value);
        public abstract int virTypedparametersGetULLong(_virTypedParameter* parameters, int nparameters, StringBuilder name, ref ulong value);
        public abstract int virTypedparametersGetDouble(_virTypedParameter* parameters, int nparameters, StringBuilder name, ref double value);
        public abstract int virTypedparametersGetBoolean(_virTypedParameter* parameters, int nparameters, StringBuilder name, ref int value);
        public abstract int virTypedparametersGetString(_virTypedParameter* parameters, int nparameters, StringBuilder name, byte** value);

        public abstract int virTypedparametersAddInt(_virTypedParameter** parameters, int *nparameters, int *maxparameters, StringBuilder name, int value);
        public abstract int virTypedparametersAddUInt(_virTypedParameter** parameters, int *nparameters, int *maxparameters, StringBuilder name, uint value);
        public abstract int virTypedparametersAddLLong(_virTypedParameter** parameters, int *nparameters, int *maxparameters, StringBuilder name, long value);
        public abstract int virTypedparametersAddULLong(_virTypedParameter** parameters, int *nparameters, int *maxparameters, StringBuilder name, ulong value);
        public abstract int virTypedparametersAddDouble(_virTypedParameter** parameters, int *nparameters, int *maxparameters, StringBuilder name, double value);
        public abstract int virTypedparametersAddBoolean(_virTypedParameter** parameters, int *nparameters, int *maxparameters, StringBuilder name, int value);
        public abstract int virTypedparametersAddString(_virTypedParameter** parameters, int *nparameters, int *maxparameters, StringBuilder name, StringBuilder value);
        public abstract int virTypedparametersAddStringList(_virTypedParameter** parameters, int *nparameters, int *maxparameters, StringBuilder name, byte** values);
        public abstract int virTypedparametersAddFromString(_virTypedParameter** parameters, int *nparameters, int *maxparameters, StringBuilder name, int type, StringBuilder value);

        public abstract void virTypedparametersClear(_virTypedParameter* parameters, int nparameters);
        public abstract void virTypedparametersFree(_virTypedParameter* parameters, int nparameters);
    }
}